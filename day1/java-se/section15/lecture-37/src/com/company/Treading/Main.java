package com.company.Treading;

public class Main {

    public static void main(String[] args) {

        Sequence sequence = new Sequence();

        Worker worker = new Worker(sequence);
        worker.start();


        Worker worker2 = new Worker(sequence);
        worker2.start();

        for (int i = 0; i < 100; i++) {
            System.out.println(sequence.getNext());
        }
    }
}

class Worker extends Thread {
    Sequence sequence;

    public Worker(Sequence sequence) {
        this.sequence = sequence;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName() + " get value: " + sequence.getNext());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
