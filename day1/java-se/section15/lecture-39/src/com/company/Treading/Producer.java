package com.company.Treading;

import java.util.List;

public class Producer implements Runnable{
    List<Integer> questionList = null;
    final int limit = 5;
    private int questionNo = 1;


    public Producer(List<Integer> questionList){
        this.questionList = questionList;

    }

    public void readQuestion(int questionNo) throws InterruptedException {
        synchronized (questionList){
            while (questionList.size() == limit){
                System.out.println("Question have piled up... wait for answers");
                questionList.wait();
            }
        }

        synchronized (questionList){
            System.out.println("New question: "+questionNo);
            questionList.add(questionNo++);
            questionList.add(questionNo++);
            Thread.sleep(100);
            questionList.notify();
        }

    }

    @Override
    public void run() {
        try {
            readQuestion(questionNo++);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
