package com.company.Treading;


import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> questionList = new ArrayList<Integer>();

        Thread thread1 = new Thread(new Producer(questionList));
        Thread thread2 = new Thread(new Consumer(questionList));

        thread1.start();
        thread2.start();
    }
}

