package com.company.Treading;

import com.company.Treading.inventory.InventoryManager;

public class Main {

    public static void main(String[] args) {
        InventoryManager inventoryManager = new InventoryManager();
        Thread inverntoryTask = new Thread(new Runnable() {
            @Override
            public void run() {
                inventoryManager.populateSoldProducts();
            }
        });

        Thread displayTask = new Thread(new Runnable() {
            @Override
            public void run() {
                inventoryManager.dispalaySoldProductList();
            }
        });

        inverntoryTask.start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        displayTask.start();
    }
}

