package com.company.Treading.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class InventoryManager {
    List<Product> soldProductList = new CopyOnWriteArrayList<>();
    List<Product> purchasedProductList = new ArrayList<>();

    public void populateSoldProducts(){
        for (int i = 0; i < 100; i++) {
            Product prod = new Product(i, "test_product_"+i);
            soldProductList.add(prod);

            System.out.println("Added: " + prod);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void dispalaySoldProductList(){
        for (Product product: soldProductList){
            System.out.println("PRONTING THE SOLD: " + product);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
