package com.company.Treading;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    BlockingQueue<Integer> questionQueue;

    final int limit = 5;
    private int questionNo = 1;


    public Producer(BlockingQueue<Integer> questionQueue) {
        this.questionQueue = questionQueue;
    }


    @Override
    public void run() {

        while (true) {
            try {
                synchronized (this) {
                    int nextQuestion = questionNo++;
                    System.out.println("GOT new question: " + nextQuestion);
                    questionQueue.put(nextQuestion);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
