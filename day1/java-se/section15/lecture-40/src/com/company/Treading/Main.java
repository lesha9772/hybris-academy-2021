package com.company.Treading;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Main {

    public static void main(String[] args) {

        BlockingQueue<Integer> questionOne = new ArrayBlockingQueue<Integer>(5);

        Thread t1 = new Thread(new Producer(questionOne));
        Thread t2 = new Thread(new Consumer(questionOne));

        t1.start();
        t2.start();
    }
}

