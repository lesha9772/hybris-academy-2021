package com.company.Treading;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        Task taskRunner = new Task("Thread A");
        taskRunner.start();

        Task taskRunner2 = new Task("Thread B");
        taskRunner2.start();

        Thread taskRunner4 = new Thread(new Task("Thread C"));
        taskRunner4.start();


        Thread taskRunner5 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("number " + i + " - " + Thread.currentThread().getName());
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        taskRunner5.start();

        System.out.println("Hello there...");
    }
}

class Task extends Thread {
    String name;

    public Task(String name){

        this.name = name;
    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("number " + i + " - " + Thread.currentThread().getName());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
