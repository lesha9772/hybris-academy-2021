package com.company.lesson2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> animals = new ArrayList<String>();
        animals.add("Lion");
        animals.add("Cat");
        animals.add("Dog");
        animals.add("Bird");

        for (int i = 0; i < animals.size(); i++) {
            System.out.println(animals.get(i));
        }

        for (String value : animals) {
            System.out.println(value);
        }

        List<Vehicle> vehicles = new LinkedList<>();
        Vehicle vehicle2 = new Vehicle("Jeep", "Wtangler", 2000, true);
        vehicles.add(new Vehicle("Honda", "Accord", 12000, false));
        vehicles.add(vehicle2);

        for (Vehicle vehicle : vehicles){
            System.out.println(vehicle);
        }

        prinElements(animals);
        prinElements(vehicles);
    }

    public static void prinElements(List someList){
        for (int i = 0; i < someList.size(); i++) {
            System.out.println(someList.get(i));
        }
    }
}
