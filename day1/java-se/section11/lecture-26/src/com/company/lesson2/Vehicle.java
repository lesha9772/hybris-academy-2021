package com.company.lesson2;

public class Vehicle {
    String make;
    String modle;
    int price;
    boolean fourWDrite;

    public Vehicle(String make, String modle, int price, boolean fourWDrite) {
        super();
    this.make = make;
    this.modle = modle;
    this.price = price;
    this.fourWDrite = fourWDrite;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModle() {
        return modle;
    }

    public void setModle(String modle) {
        this.modle = modle;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isFourWDrite() {
        return fourWDrite;
    }

    public void setFourWDrite(boolean fourWDrite) {
        this.fourWDrite = fourWDrite;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "make='" + make + '\'' +
                ", modle='" + modle + '\'' +
                ", price=" + price +
                ", fourWDrite=" + fourWDrite +
                '}';
    }
}
