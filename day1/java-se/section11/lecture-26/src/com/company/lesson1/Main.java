package com.company.lesson1;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        ArrayList words = new ArrayList();
        words.add("hello");
        words.add("there");
        words.remove("there");
        String item1 = (String) words.get(0);

        System.out.println(item1);


        ArrayList<String> words2 = new ArrayList<String>();
        words2.add("again");
        words2.add("hello");
        words2.add("there");
        words2.remove("there");
        String item2 = words2.get(1);

        System.out.println(item2);


        LinkedList<Integer> numbers = new LinkedList<Integer>();
        numbers.add(100);
        numbers.add(200);
        numbers.add(45);
        numbers.add(1000);
        numbers.removeFirst();

        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
