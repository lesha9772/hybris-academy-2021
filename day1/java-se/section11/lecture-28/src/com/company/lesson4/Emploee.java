package com.company.lesson4;

public class Emploee implements Comparable<Emploee> {
    String name;
    int salary;
    String department;

    public Emploee(String name, int salary, String department) {
        this.name = name;
        this.salary = salary;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }


    @Override
    public String toString() {
        return "Emploee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", department='" + department + '\'' +
                '}';
    }

    @Override
    public int compareTo(Emploee o) {
        if (this.salary < o.salary) {
            return -1;
        } else if (this.salary > o.salary) {
            return 1;
        }
        return 0;
    }
}
