package com.company.lesson4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<Integer>();

        list1.add(10);
        list1.add(12);
        list1.add(14);
        list1.add(15);
        list1.add(17);
        list1.add(19);
        list1.add(10);
        list1.add(10);
        list1.add(10);
        list1.add(10);

        System.out.println(list1);
        list1.clear();
        System.out.println(list1);

        ArrayList<Integer> newList = new ArrayList<Integer>();
        newList.add(17);
        newList.add(19);
        newList.add(10);
        System.out.println(newList);

        boolean hasValue = newList.contains(17);
        System.out.println(hasValue);

        boolean isEmpty = newList.isEmpty();
        System.out.println(isEmpty);


        HashSet<Integer> hashSet1 = new HashSet<Integer>();

        hashSet1.add(10);
        hashSet1.add(12);
        hashSet1.add(14);
        hashSet1.add(15);
        hashSet1.add(17);
        hashSet1.add(19);
        hashSet1.add(10);
        hashSet1.add(10);
        hashSet1.add(10);
        hashSet1.add(10);

        ArrayList<Integer> myList = new ArrayList<Integer>(hashSet1);
        System.out.println(myList);

        Collections.sort(myList);
        System.out.println(myList);


        HashSet<String> hashSet2 = new HashSet<String>();

        hashSet2.add("Hello");
        hashSet2.add("Random");
        hashSet2.add("Animal");

        ArrayList<String> myListString = new ArrayList<String>(hashSet2);
        System.out.println(myListString);

        Collections.sort(myListString);
        System.out.println(myListString);


        HashSet<Emploee> hashSet3 = new HashSet<Emploee>();
        Emploee dev1 = new Emploee("Mike", 3500, "Senior developer");
        Emploee dev2 = new Emploee("Nick", 2500, "Middle developer");
        Emploee dev3 = new Emploee("Nick", 1500, "Junior developer");

        hashSet3.add(dev1);
        hashSet3.add(dev2);
        hashSet3.add(dev3);
        System.out.println(hashSet3);

        ArrayList<Emploee> emploees = new ArrayList<Emploee>(hashSet3);

        Collections.sort(emploees);
        System.out.println(emploees);


//
//
//        for (Animal animal : animals) {
//            System.out.println(animal);
//        }

    }
}
