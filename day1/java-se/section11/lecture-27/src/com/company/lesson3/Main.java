package com.company.lesson3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        HashSet<Animal> animals = new HashSet<Animal>();
        Animal dog12 = new Animal("Dog", 12);
        Animal dog11 = new Animal("Dog", 11);

        animals.add(dog11);
        animals.add(dog12);
        animals.add(new Animal("Cat", 22));
        animals.add(new Animal("Bird", 32));


        for (Animal animal : animals) {
            System.out.println(animal);
        }

    }
}
