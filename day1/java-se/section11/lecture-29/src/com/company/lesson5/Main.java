package com.company.lesson5;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        HashMap<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("title1", "Some text 1");
        dictionary.put("title2", "Some text 2");
        dictionary.put("title3", "Some text 3");

        for (String word : dictionary.keySet()) {
            System.out.println(word);
        }
        for (Map.Entry <String, String> entry : dictionary.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }



        TreeMap<String, String> dictionary1 = new TreeMap<String, String>();
        dictionary1.put("title1", "Some text 1");
        dictionary1.put("title2", "Some text 2");
        dictionary1.put("title3", "Some text 3");
        dictionary1.put("title3", "***********");
        dictionary1.put("title1", "***********");

        for (String word1 : dictionary1.values()) {
            System.out.println(word1);
        }

        for (String word1 : dictionary1.values()) {
            System.out.println(word1);
        }

//
//
//        for (Animal animal : animals) {
//            System.out.println(animal);
//        }

    }
}
